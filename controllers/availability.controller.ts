import { getReservations, getTimetables } from './../services/availability.service';
import express from 'express';
import moment from 'moment';
import { isDateIn } from '../helper/helper';

const router = express.Router();

router.get('/availability', async (req, res) => {
    var date : any = req.query.date;
    var resourceId : any = req.query.resourceId;

    const timetables: [] = await getTimetables(date, resourceId);
    const isDateInOpeningAndClosingRange = timetables.some(({opening, closing}) => isDateIn(opening, closing, moment(date).unix()));

    if (isDateInOpeningAndClosingRange) {
        const reservations: [] = await getReservations(date, resourceId);
        const alreadyTaken = reservations.some(({reservationStart, reservationEnd}) => isDateIn(reservationStart, reservationEnd, moment(date).unix()));

        if (!alreadyTaken) {
            return res.json({ "available": true});
        }
        return res.json({ "available": false});

    }
    else {
        res.json({ "available": false});
    }
});

// Error handling
router.use('/availability', (err, req, res, next) => {
    if ( err.response.data) {
        return res.status(400).send(err.response.data);
    }
    res.status(500).send(err);
});

export = router;