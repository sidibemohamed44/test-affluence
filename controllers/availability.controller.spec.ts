import moment from 'moment';
import request from 'supertest';
import * as service from '../services/availability.service';

jest.mock('../services/availability.service');

import { app } from './../index';

describe('availability controller', () => {

    describe('normal behavior', () => {
        beforeEach(() => {
            (service.getReservations as any).mockReset();
            (service.getTimetables as any).mockReset();
        });
    
        it('should return false when date is not in opening and closing range', async () => {
            // Given
            const timetables = [{opening: moment().unix(), closing: moment().add(4, 'hour').unix()}];
            const reservations = [{reservationStart: moment().unix(), reservationEnd: moment().add(3, 'hour').unix()}];
            const resourceId = 'whatever';
            const date = moment().add(5, 'hour').format();
            (service.getReservations as any).mockResolvedValue([reservations]);
            (service.getTimetables as any).mockResolvedValue(timetables);
            const expectedBody = { available: false };
    
            // When | Then
            await request(app)
                .get('/api/availability')
                .query({date, resourceId})
                .expect(200)
                .then((res) => expect(expectedBody).toEqual(res.body));
        });
    
        it('should return false when date is in opening and closing range but reservation already taken', async () => {
            // Given
            const timetables = [{opening: moment().unix(), closing: moment().add(4, 'hour').unix()}];
            const reservations = [{reservationStart: moment().unix(), reservationEnd: moment().add(3, 'hour').unix()}];
            const resourceId = 'whatever';
            const date = moment().add(2, 'hour').format();
            (service.getReservations as any).mockResolvedValue(reservations);
            (service.getTimetables as any).mockResolvedValue(timetables);
            const expectedBody = { available: false };
    
            // When | Then
            await request(app)
                .get('/api/availability')
                .query({date, resourceId})
                .expect(200)
                .then((res) => expect(expectedBody).toEqual(res.body));
        });
    
        it('should return true when date is in opening and closing range but and is free for reservation', async () => {
            // Given
            const timetables = [{opening: moment().unix(), closing: moment().add(4, 'hour').unix()}];
            const reservations = [{reservationStart: moment().unix(), reservationEnd: moment().add(2, 'hour').unix()}];
            const resourceId = 'whatever';
            const date = moment().add(3, 'hour').format();
            (service.getReservations as any).mockResolvedValue(reservations);
            (service.getTimetables as any).mockResolvedValue(timetables);
            const expectedBody = { available: true };
    
            // When | Then
            await request(app)
                .get('/api/availability')
                .query({date, resourceId})
                .expect(200)
                .then((res) => expect(expectedBody).toEqual(res.body));
        });
    });

    describe('error', () => {
        it('status code should be 400', async () => {
            // Given
            const errorMessage = 'some error';
            const resourceId = 'whatever';
            const date = moment().add(3, 'hour').format();
            (service.getTimetables as any).mockRejectedValue({response: { data: errorMessage }});
    
            // When | Then
            await request(app)
                .get('/api/availability')
                .query({date, resourceId})
                .expect(400)
                .then((res) => expect(errorMessage).toEqual(res.text));
        });

        it('status code should be 500', async () => {
            // Given
            const resourceId = 'whatever';
            const date = moment().add(3, 'hour').format();
            (service.getTimetables as any).mockRejectedValue('error');
            const expectedBody = { available: true };
    
            // When | Then
            await request(app)
                .get('/api/availability')
                .query({date, resourceId})
                .expect(500)
        });
    })
    

});