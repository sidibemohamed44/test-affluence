export const isDateIn = (start, end, date) => {
    return date >= start && date < end;
  }
