import moment from 'moment';
import { isDateIn } from './helper';

describe('isDateIn', () => {
    it('should return true when date is in range', () => {
        const start = moment().unix() - 1;
        const end = moment().unix() + 1;
        const date = moment().unix();

        expect(isDateIn(start, end, date)).toBeTruthy();
    });
    it('should return false when date is not in range', () => {
        const start = moment().unix() - 1;
        const end = moment().unix() + 1;
        const date = moment().unix() + 2;

        expect(isDateIn(start, end, date)).toBeFalsy();
    });
});