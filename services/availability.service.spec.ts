import axios from "axios";
import moment from "moment";
import { getReservations, getTimetables } from "./availability.service";

describe('availability service', () => {
    describe('get timetables', () => {
        it('should return closing and opening times from reservation service api as unix timestamp when prop open is true', async () => {
            // Given
            const opening = moment();
            const closing = moment().add(1, 'hour');
            const data = {open: true, timetables: [{opening, closing}]}
            jest.spyOn(axios, 'get').mockResolvedValue(Promise.resolve({data}));
            const expectedResult = [{opening: moment(opening).unix(), closing: moment(closing).unix()}];
            
            // When
            const result = await getTimetables(new Date(), 'fakeId');

            // Then
            expect(result).toEqual(expectedResult);
        });

        it('should empty array when prop open is false', async () => {
            // Given
            const data = {open: false};
            jest.spyOn(axios, 'get').mockResolvedValue(Promise.resolve({data}));
            const expectedResult = [];
            
            // When
            const result = await getTimetables(new Date(), 'fakeId');

            // Then
            expect(result).toEqual(expectedResult);
        });
    });

    describe('get reservations', () => {
        it('should return reservations from reservation service api as unix timestamp', async () => {
            // Given
            const reservationStart =  moment();
            const reservationEnd = moment().add(1, 'hours');
            const data = { reservations: [{reservationStart, reservationEnd} ]};
            jest.spyOn(axios, 'get').mockResolvedValue(Promise.resolve({data}));
            const expectedResult = [{reservationStart: moment(reservationStart).unix(), reservationEnd: moment(reservationEnd).unix()}];
            
            // When
            const result = await getReservations(new Date(), 'fakeId');

            // Then
            expect(result).toEqual(expectedResult);
        });
    });
})