import axios from "axios";
import moment from "moment";
import { reservartionServiceUrl } from "../config";

export const getTimetables = (date, resourceId) => axios.get(`${reservartionServiceUrl}/timetables`, {params : { date, resourceId}})
    .then((res) => {
        if (res.data.open) {
            return res.data.timetables.map(({opening, closing}) => ({
                opening: moment(opening).unix(),
                closing: moment(closing).unix()
            }));
        }
        return [];
    });

export const getReservations = (date, resourceId) => axios.get(`${reservartionServiceUrl}/reservations`, {params : { date, resourceId}})
    .then((res) => res.data.reservations.map(({reservationStart, reservationEnd}) => ({
        reservationStart: moment(reservationStart).unix(),
        reservationEnd: moment(reservationEnd).unix()
    })));
