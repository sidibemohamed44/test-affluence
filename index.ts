import express from 'express';
import 'express-async-errors';
import cors from 'cors';

import availabilityController from './controllers/availability.controller';
import { port } from './config';

export const app = express();

app.use(cors())
app.use('/api', availabilityController);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});