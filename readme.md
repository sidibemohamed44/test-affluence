# Installation and start

## steps:
* clone project from git@gitlab.com:sidibemohamed44/test-affluence.git
* install the dependencies from package.json
* launch project on localhost:3000

## commands:
```
$ npm install
$ npm start
```

## unit-tests:
```
$ npm test
```

**Attention**: Affluence reservation service from https://gitlab.com/affluences/affluences-tests/reservation-service has to be launched first on localhost:8080.
